// Interfaz que describe la Info del Juego para las Tarjetas
export interface ItemGame {
  gameID: string,
  steamAppID: string,
  cheapest: string,
  cheapestDealID: string,
  external: string,
  internalName: string,
  thumb: string
}
