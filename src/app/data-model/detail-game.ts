// Interfaz de las Ventas Realizadas incluidas en los Detalles del Juego
interface Deal {
  storeID: string,
  dealID: string,
  price: string,
  retailPrice: string,
  savings: string
}

// Interfaz los Detalles del Juego
export interface DetailGame {
  info: { 
    title: string,
    steamAppID: string | null,
    thumb: string,
  },
  cheapestPriceEver: {
    price: string,
    date: number,
  },
  deals: Deal[]
}
