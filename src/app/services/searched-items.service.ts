import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

// Servicio usuado para la busqueda de juegos por coincidencia de Texto
@Injectable({
  providedIn: 'root'
})
export class SearchedItemsService {

  // URL del punto de destino para la solicitud a la API
  urlFilterGames: string = environment.API_URL + 'games?limit=10&exact=0&title=';

  constructor(private httpClient: HttpClient) { }

  // Función que devuelve el Observable que realiza la solictud de la Info de los Juegos a buscar
  lookupGamesByText(searchedName: string): Observable<any> {
    const gameName = searchedName.toLowerCase();
    return this.httpClient.get<any>(this.urlFilterGames + gameName);
  }
}
