import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

// Servicios usados para la solicitud de Info de los Detalles de Juego y Tiendas
@Injectable({
  providedIn: 'root'
})
export class ItemDetailsService {

  // URL's que aputan el Punto de Destino para las solicitudes a la API
  urlGameId: string = environment.API_URL + 'games?id=';
  urlStoresData: string = environment.API_URL + 'stores';
  
  constructor(private httpClient: HttpClient) { }

  // Función que devuelve el Observable para la solicitud del juego seleccionado por Tarjeta
  findGameById(gameId: string): Observable<any> {
    return this.httpClient.get<any>(this.urlGameId + gameId);
  }

  // Función que devuelve el Observable para la solicitud de la Info de Tiendas
  getDataGameStores(): Observable<any> {
    return this.httpClient.get<any>(this.urlStoresData);
  }
}
