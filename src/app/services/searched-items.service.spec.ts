import { TestBed } from '@angular/core/testing';

import { SearchedItemsService } from './searched-items.service';

describe('SearchedItemsService', () => {
  let service: SearchedItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchedItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
