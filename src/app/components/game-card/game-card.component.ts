import { Component, Input, Output, EventEmitter } from '@angular/core';

// Componente Objeto de la Tarjeta de Juego
@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent {

  // Info que recibe la Tarjeta para renderizar
  @Input() gameID: string = '';
  @Input() titleGame: string = '';
  @Input() imgThumb: string = '';
  @Input() internalName: string = '';

  // Enlace del evento para la emisión del Juego y los detalles
  @Output() gamePicker = new EventEmitter<string>();

  constructor() { }

  // Evento que emite la tarjeta para seleccionar el Juego con los Detalles
  onClickViewDetails(event: PointerEvent | any): void {
    this.gamePicker.emit(event.target.id);
    setTimeout(() => { this.gamePicker.emit('') }, 200);
  }

}
