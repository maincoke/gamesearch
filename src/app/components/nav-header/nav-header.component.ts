import { Component, OnInit, EventEmitter, Output, HostListener } from '@angular/core';

@Component({
  selector: 'app-nav-header',
  templateUrl: './nav-header.component.html',
  styleUrls: ['./nav-header.component.scss']
})
export class NavHeaderComponent implements OnInit {

  textToFindGame: string = '';

  @Output() sendTextGame = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void { }

  searchGameText(): void {
    if (this.textToFindGame) {
      this.textToFindGame = this.textToFindGame.toLowerCase();
      this.sendTextGame.emit(this.textToFindGame);
    }
  }

  @HostListener("window:scroll", [ '$event' ]) trackScrollOnNavbar(evnt: Event) {
    const logo = document.querySelectorAll('.logo-sticky')[0].children[0];
    if (window.scrollY > 90 && (document.body.clientWidth > 580)) {
      logo.classList.add('shrink');
    } else {
      logo.classList.remove('shrink');
    }
  }
}
