import { Component, OnInit } from '@angular/core';
import { SearchedItemsService } from 'src/app/services/searched-items.service';
import { ItemGame } from '../../data-model/item-game';

// Componente Objeto del contenedor de las tarjetas encontradas y mensajes de busqueda
@Component({
  selector: 'app-items-container',
  templateUrl: './items-container.component.html',
  styleUrls: ['./items-container.component.scss']
})
export class ItemsContainerComponent implements OnInit {

  // Info y datos que controlan las búsquedas y las selecciones de los juegos
  foundGames: Array<ItemGame> = [];
  textGame: string = '';
  idGamePicked: string = '';
  fireModal: boolean = false;

  constructor(private dataItemGamesSrv: SearchedItemsService) { }

  ngOnInit(): void { }

  // Evento que ejecuta la búsqueda por el texto ingresado y filtra los juegos
  renderSearchGames(gameText: string): void {
    this.dataItemGamesSrv.lookupGamesByText(gameText).subscribe((data: Array<ItemGame>) => {
      this.foundGames = data.length !== 0 ? data : [];
      this.textGame = gameText;
    });
  }

  // Evento que captura la tarjeta seleccionada del juego
  gettingIdGame(gameId: string): void {
    if (gameId) {
      this.fireModal = true;
      this.idGamePicked = gameId;
    } else {
      this.fireModal = false;
    }
  }
}