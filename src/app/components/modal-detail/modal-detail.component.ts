import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ItemDetailsService } from 'src/app/services/item-details.service';
import { DetailGame } from 'src/app/data-model/detail-game';

// Componente Objeto de la Ventana Modal
@Component({
  selector: 'app-modal-detail',
  templateUrl: './modal-detail.component.html',
  styleUrls: ['./modal-detail.component.scss']
})
export class ModalDetailComponent implements OnChanges, OnInit {

  // Info de juego y confirmación de apertura de Ventana Modal
  @Input() gamePicked: string = '';
  @Input() openModal: boolean = false;

  // Atributo que almacena los detalles del juego a mostrar en Ventana Modal
  gameDetail: DetailGame = {
    info: { title: "", steamAppID: null, thumb: "" },
    cheapestPriceEver: { price: "", date: 0 },
    deals: [ { storeID: "", dealID: "", price: "", retailPrice: "", savings: "" } ]
  };

  // Atributos auxiliares para la apertura de Ventana modal y Datos de Tiendas
  storesData: Array<any> = [];
  showDetailsModal: string = '';

  constructor(private dataDetailGameSrv: ItemDetailsService) { }

  // Evento que escucha los cambios en el contenedor de tarjetas para la selección de la Tarejta de Juego
  ngOnChanges(changesProps: SimpleChanges): void {
    if (changesProps.openModal.currentValue && this.gamePicked) {
      this.dataDetailGameSrv.findGameById(this.gamePicked).subscribe((data: DetailGame) => {
        this.gameDetail = this.fixDataGame(data);
        this.showDetailsModal = 'block';
      });
    }
  }

  // Al iniciar el componente obtenemos los datos de las Tiendas
  ngOnInit(): void {
    this.dataDetailGameSrv.getDataGameStores().subscribe((data: Array<any>) => {
      this.storesData = data.map(el => new Object({ storeID: el.storeID, storeName: el.storeName }) );
    });
  }

  // Corrección de la info de los detalles del Juego - Ejecuta la relación de la Info de las Tiendas
  fixDataGame(dataGame: DetailGame): DetailGame {
    dataGame.cheapestPriceEver.date = dataGame.cheapestPriceEver.date * 1000;
    dataGame.deals.forEach(deal => {
      this.storesData.forEach(store => {
        if (deal.storeID === store.storeID) { 
          return deal.storeID = store.storeName;
        }
      });
    });
    return dataGame; 
  }

  // Evento que se ejecuta para el cierre de la Ventana Modal
  closeModalDetails(): void {
    this.showDetailsModal = 'none';
  }
}
