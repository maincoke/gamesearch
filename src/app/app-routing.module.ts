import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemsContainerComponent } from './components/items-container/items-container.component';

// Rutas utilizadas por el modulo de enrutamiento para las URL's de la Aplicación
const routes: Routes = [
  { path: 'buscar', component: ItemsContainerComponent },
  { path: '', redirectTo: 'buscar', pathMatch: 'full' },
  { path: '**', redirectTo: 'buscar' }
];

// Modulo de Enrutamiento de las URL de la Aplicación
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }