import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavHeaderComponent } from './components/nav-header/nav-header.component';
import { ItemsContainerComponent } from './components/items-container/items-container.component';
import { GameCardComponent } from './components/game-card/game-card.component';
import { ModalDetailComponent } from './components/modal-detail/modal-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavHeaderComponent,
    ItemsContainerComponent,
    GameCardComponent,
    ModalDetailComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [ HttpClient, NavHeaderComponent, ItemsContainerComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
