import { Component } from '@angular/core';

// Componente Objeto Principal de la Aplicación de Busqueda de Juegos
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gameSearch';
  
  constructor() {}
}
