// Variables de Entorno de la Aplicación en Producción
export const environment = {
  production: true,
  API_URL: "https://www.cheapshark.com/api/1.0/"
};
